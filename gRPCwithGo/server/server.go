package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"

	"app/grpc_helloworld"

	"google.golang.org/grpc"
)

type HelloworldServer struct {
	grpc_helloworld.UnimplementedHelloWorldServer
}

func (hs *HelloworldServer) SayHello(ctx context.Context, in *grpc_helloworld.ClientMessage) (*grpc_helloworld.ServerMessage, error){
	log.Printf("Received : %v", in.GetRequestMessage())
	return &grpc_helloworld.ServerMessage{
		ResponseMessage: fmt.Sprintf("Hello back to: %v",in.GetRequestMessage()),
	}, nil
}


func main() {
	portServer := flag.Int("port",5000,"The server Port")
	flag.Parse()
	list, err := net.Listen("tcp",fmt.Sprintf(":%d",*portServer))
	if err != nil {
		log.Fatalf("fail to listen: %v\n", err)
	}
	grpcServer := grpc.NewServer()
	grpc_helloworld.RegisterHelloWorldServer(grpcServer, &HelloworldServer{})
	if err := grpcServer.Serve(list); err != nil {
		log.Fatalf("Fail to server: %v\n", err)
	}
}


