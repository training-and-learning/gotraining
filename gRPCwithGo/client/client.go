package main

import (
	"app/grpc_helloworld"
	"context"
	"flag"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	addr := flag.String("addr", "localhost:5000","the address to connect to")
	messageRequest := flag.String("message","I'm Client", "the message send to server")
	flag.Parse()
	conn, err := grpc.Dial(*addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connected: %v\n", err)
	}
	defer conn.Close()
	c := grpc_helloworld.NewHelloWorldClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	respone, err := c.SayHello(ctx, &grpc_helloworld.ClientMessage{
		RequestMessage: *messageRequest,
	})
	if err != nil {
		log.Fatalf("could not get response from sever: %v",err)
	}
	log.Default().Printf("Response from server: %v", respone.ResponseMessage)
}