package main

import (
	"app/src"
	"fmt"
	"sync"
	"time"

	"golang.org/x/tour/tree"
)

func main() {
	src.ExampleGoroutines()
	fmt.Println("")
	src.ExampleOfChannel()

	// c := make(chan int)
	// tree1 := tree.New(1)
	// wr1 := new(sync.WaitGroup)
	// wr1.Add(1)
	// go Walk(tree1, c)
	// go func ()  {
	// 	loop: for {
	// 		select{
	// 		case item:= <- c:
	// 			fmt.Printf("%v", item)
	// 		case <- time.After(1000*time.Millisecond):
	// 			break loop
	// 		}	
	// 	}
	// 	wr1.Done()
	// }()
	// wr1.Wait()

	fmt.Printf("Result Test: %v \n",Same(tree.New(3), tree.New(2)))
	fmt.Printf("Result Test: %v \n",Same(tree.New(1), tree.New(1)))

}

func Walk(t *tree.Tree, ch chan int){
	if t.Left != nil {
		Walk(t.Left, ch)
	} 
	ch <- t.Value
	if t.Right != nil {
		Walk(t.Right, ch)
	} 
}

func Same(t1, t2 *tree.Tree) bool{
	c1 := make(chan int)
	c2 := make(chan int)
	slice1 := make([]int, 0, 10)
	slice2 := make([]int, 0, 10)
	wr := new(sync.WaitGroup) 
	wr.Add(1)
	go Walk(t1, c1)
	go Walk(t2, c2)
	go func (){	
		loop : for {
			select {
			case item := <- c1:
				fmt.Printf("%v ", item)
				slice1 = append(slice1, item)
			case item := <- c2:
				fmt.Printf("%v ", item)
				slice2 = append(slice2, item)
			case <- time.After(1000*time.Millisecond):
				break loop
			}
		}
		wr.Done()
	}()
	wr.Wait()
	fmt.Println("")
	fmt.Printf("Slice1: %v \n Slice2: %v \n",slice1,slice2)
	for i := 0; i < 10; i++ {
		if slice1[i] != slice2[i] {
			return false
		}
	}
	return true
}


