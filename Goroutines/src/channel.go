package src

import "fmt"

func ExampleOfChannel() {
	a := []int{7, 2, 8, -9, 4, 0}
	ch := make(chan int)
	go sum(a[:len(a)/2], ch)
	go sum(a[len(a)/2:], ch)
	x,y := <- ch,<- ch
	fmt.Println(x, y, x+y)
}

func ExampleOfBufferChannel(){
	
}

func sum(a []int, ch chan int) {
	sum := 0
	for _, v := range a {
		sum += v
	}
	ch <- sum
}
