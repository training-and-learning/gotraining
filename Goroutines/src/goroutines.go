package src

import (
	"fmt"
	"sync"
	"time"
)

var theNumberWillBeAccessByMultilThread *int = new(int)

func ExampleGoroutines() {
	wr := new(sync.WaitGroup)
	wr.Add(1)
	go say("you", wr)
	fmt.Println("helloworld")
	wr.Wait()

	fmt.Println("")

	wr.Add(7)
	go editTheNumber(3, wr)
	go func ()  {
		newNumber := 9
		theNumberWillBeAccessByMultilThread = &newNumber
		wr.Done()
	}()
	go editTheNumber(-1, wr)
	go editTheNumber(-12, wr)
	go func ()  {
		fmt.Printf("The number: %v\n",*theNumberWillBeAccessByMultilThread)
		wr.Done()

	}()
	go editTheNumber(16, wr)
	go editTheNumber(-10, wr)
		wr.Wait()
}

func say(message string, wr *sync.WaitGroup) {
	time.Sleep(5000*time.Millisecond)
	fmt.Printf("Say: %v", message)
	wr.Done()
}

func editTheNumber(newNumber int, wr *sync.WaitGroup){
	theNumberWillBeAccessByMultilThread = &newNumber
	wr.Done()
}