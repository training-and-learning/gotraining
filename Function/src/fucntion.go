package theFunction

import "fmt"

type User struct{
	FirstName, LastName string
}

func (u User) Greeting() string{
	u.LastName = "this change not apply to the origin object because the parameter is u User the change is apply to the coppy object from u User"
	return fmt.Sprintf("Dear %s %s", u.FirstName, u.LastName)
}

func (u *User) SayHi() string {
	u.LastName = "this will change the last name of the User"
	return fmt.Sprintf("Dear %s %s", u.FirstName, u.LastName)
}

func ExampleFunction(){

	u := User{"Matt","Aimonetti"}
	fmt.Println(u.Greeting())
	fmt.Printf("Last name is: %s\n", u.LastName)
	fmt.Println(u.SayHi())
	fmt.Printf("Last name is: %s\n", u.LastName)
}