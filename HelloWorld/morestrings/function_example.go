package morestrings

import "fmt"

func TestVoidFunction(){
	fmt.Println("This is void function")
}

func thisFunctionCantBeExport(){
	fmt.Println("Not export function")
}

func TestFunctionWithReturn() string {
	thisFunctionCantBeExport()
	return "return Hello"
}

func TestFunctionWithReturnTwoVari() (string, int){
	return "Heelo", 123
}

func TestVoidFunctionWithParameter(a,b string){
	fmt.Printf("%v-%v\n", a,b)
}