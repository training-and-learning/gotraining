package main

/**
imprt in go
*/
import (
	"example/user/hello/morestrings"
	"fmt"

	"rsc.io/quote"
)

func main() {

	/**
	Variable declare in go 
	*/
	var var1 int
	var1 = 123

	var2:=456

	var (
		var3 string
		var4 float32
	)
	var3 = "hello"
	var4 = 12.54

	var (
		var5 string = "world"
		var6 float64 = 56.79
	)

	var7, var8:="Hello world", 1235.89

	fmt.Printf("%T(%v)\n",var1,var1)
	fmt.Printf("%T(%v)\n",var2,var2)
	fmt.Printf("%T(%v)\n",var3,var3)
	fmt.Printf("%T(%v)\n",var4,var4)
	fmt.Printf("%T(%v)\n",var5,var5)
	fmt.Printf("%T(%v)\n",var6,var6)
	fmt.Printf("%T(%v)\n%T(%v)\n",var7,morestrings.ReverseRunes(var7), var8,var8)
	morestrings.TestVoidFunctionWithParameter("hello","world")
	morestrings.TestVoidFunction()
	fmt.Println(morestrings.TestFunctionWithReturn())
	morestringsVar1, morestringsVar2 := morestrings.TestFunctionWithReturnTwoVari()
	fmt.Printf("%v%v",morestringsVar1,morestringsVar2)
	fmt.Println(quote.Go())

}