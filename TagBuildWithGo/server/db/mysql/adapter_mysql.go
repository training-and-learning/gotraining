//go:build mysql

package mysql

import (
	store "app/server/store"
	"fmt"
)

type MysqlDb struct {
	Name string
}

func (db *MysqlDb) GetAdapterNameType() string {
	return db.Name
}

func init(){
	fmt.Println("Build with tag mysql")
	store.RegisterAdapter(&MysqlDb{
		Name: "MYSQL",
	})
}