//go:build mongo

package mongo

import (
	store "app/server/store"
	"fmt"
)

type MongoDb struct {
	Name string
}

func (db *MongoDb) GetAdapterNameType() string {
	return db.Name
}

func init(){
	fmt.Println("Build with tag mongo")
	store.RegisterAdapter(&MongoDb{
		Name: "MONGO",
	})
}