package store

import (
	adapPack "app/server/db"
	"fmt"
)

var MyAdapter = make(map[string]adapPack.Adpater)

func ShowAdapterInitResult() {
	if len(MyAdapter) != 0 {
		for key,value := range MyAdapter {
			fmt.Printf("[%v]=%v\n", key, value.GetAdapterNameType())
		}
	} else {
		fmt.Println("adapter were not init")
	}	
}


func RegisterAdapter(a adapPack.Adpater) {
	if a != nil {
		switch a.GetAdapterNameType() {
		case "MYSQL":
			MyAdapter["mysql adapter"] = a
		case "MONGO":
			MyAdapter["mongo adapter"] = a
		}
	}
}