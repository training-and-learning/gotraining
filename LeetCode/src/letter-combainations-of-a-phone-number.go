package src

import (
	"bytes"
	"strconv"
)

func letterCombinations(digits string) []string {
	result := make([]string, 0, 10)
	choosesArr := make([]string, 0, 4)
	for i := 0; i < len(digits); i++ {
		number,_ := strconv.Atoi(string(digits[i]))
		switch number {
		case 2:
			choosesArr = append(choosesArr, "abc")
		case 3:
			choosesArr = append(choosesArr, "def")
		case 4:
			choosesArr = append(choosesArr, "ghi")
		case 5:
			choosesArr = append(choosesArr, "jkl")
		case 6:
			choosesArr = append(choosesArr, "mno")
		case 7:
			choosesArr = append(choosesArr, "pqrs")
		case 8:
			choosesArr = append(choosesArr, "tuv")
		case 9:
			choosesArr = append(choosesArr, "wxyz")
		}
	}
	
	requireLength := len(digits)
	if(requireLength < 4){
        for i:=0; i < 4- requireLength; i++ {
            choosesArr = append(choosesArr, "")
        }
    }
	for i := 0; i < len(choosesArr[0]); i++ {
		if requireLength == 1 {
			builder := new(bytes.Buffer)
			builder.WriteByte(choosesArr[0][i])
			result = append(result, builder.String())
			continue
		}
		for j := 0; j < len(choosesArr[1]); j++ {
			if requireLength == 2 {
				builder := new(bytes.Buffer)
				builder.WriteByte(choosesArr[0][i])
				builder.WriteByte(choosesArr[1][j])
				result = append(result, builder.String())
				continue
			}
			for n := 0; n < len(choosesArr[2]); n++ {
				if requireLength == 3 {
					builder := new(bytes.Buffer)
					builder.WriteByte(choosesArr[0][i])
					builder.WriteByte(choosesArr[1][j])
					builder.WriteByte(choosesArr[2][n])
					result = append(result, builder.String())
					continue
				}
				for m := 0; m < len(choosesArr[3]); m++ {
					builder := new(bytes.Buffer)
					builder.WriteByte(choosesArr[0][i])
					builder.WriteByte(choosesArr[1][j])
					builder.WriteByte(choosesArr[2][n])
					builder.WriteByte(choosesArr[3][m])
					result = append(result, builder.String())
					continue
				}
			}
		}		
	}
	return result
}