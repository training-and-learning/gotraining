package src

import (
	"strconv"
	"strings"
)

func RotatedDigits(n int) int {
	result := 0
	for i := 1; i <= n; i++ {
		if numString := strconv.Itoa(i); strings.Contains(numString, "2") || strings.Contains(numString, "5") || strings.Contains(numString, "6") || strings.Contains(numString, "9") {
			result++
		}
	}
	return result
}