package src

type ListNode struct {
	Val  int
	Next *ListNode
}

func swapPairs(head *ListNode) *ListNode {
	p1 := head
	if p1 == nil {
		return head
	}
	if p1.Next == nil {
		return p1
	}
	p2 := head.Next

	p1.Next = p2.Next
	p2.Next = p1
	head = p2

	pBefore1 := p1
	p1 = p1.Next
	if p1 == nil {
		return head
	}
	p2 = p1.Next
	if p2 == nil {
		return head
	}

	for {
		if p2.Next == nil {
			pBefore1.Next = p2
			p2.Next = p1
			p1.Next = nil
			break
		} else {
			p2Next := p2.Next
			pBefore1.Next = p2
			p2.Next = p1
			p1.Next = p2Next

			pBefore1 = p1
			p1 = p1.Next
			p2 = p1.Next
			if p1 == nil || p2 == nil {
				break
			}
		}
	}

	return head
}