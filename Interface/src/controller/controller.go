package theController

import (
	"fmt"
)

type AppController struct {
	Id string
}

func (c *AppController) showInfoController() {
	fmt.Printf("The controller info %v\n", c.Id)
}

func (c *AppController) SayHi(message string){
	c.showInfoController()
	fmt.Printf("Hi: %v \n From Controller: %v\n",message, c.Id)
}

func (c *AppController) Response(name string){
	c.showInfoController()
	fmt.Printf("Response to: %v\n From Controller: %v\n", name, c.Id)
}

