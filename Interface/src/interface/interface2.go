package theInterface

import (
	"fmt"
	"os"
)

type Reader interface{
	Read(b []byte) (n int, err error)
}

type Writer interface{
	Write(b []byte) (n int, err error)
}

type ReadWriter interface {
	Reader
	Writer
}

func ExampleInterface2(){
	var w ReadWriter = os.Stdout
	fmt.Fprintf(w, "hello, writer \n")
}