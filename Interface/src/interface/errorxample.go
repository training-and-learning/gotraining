package theInterface

import (
	"fmt"
	"time"
)

type MyError struct{
	When time.Time
	What string
}

func (e *MyError) Error() string {
	return fmt.Sprintf("at %v %s", e.When, e.What)
}

func run() error {
	return &MyError{
		When: time.Now(),
		What: "It didn't work!",
	}
}

func ErrorExample(){
	if err := run(); err != nil {
		fmt.Println(err)
	}
}

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	return fmt.Sprintf("cannot Sqrt negative number:%f", float64(e))
}

func Sqrt(x float64) (float64, error) {
	if x < 0 {
		return 0, ErrNegativeSqrt(x)
	}

	z := 1.0
	for i := 0; i < 10; i++ {
		z = z - ((z*z)-x)/(2*z)
	}
	return z, nil
}

func ErrorExample2(){
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))

}