package theInterface

type AppInterface interface {
	SayHi(message string)
	Response(name string)
}