package main

import (
	theController "app/src/controller"
	theInterface "app/src/interface"
)

func runningInterface(u theInterface.AppInterface){
	u.SayHi("Fellow")
	u.Response("phen")
}



func main(){

	theSecondController := new(theController.AppController)
	theFirstController := &theController.AppController{
		Id: "12-sd-43-qw",
	}
	theThirdController := new(theController.AppController)
	
	theSecondController.Id = "32-sd-78-we"
	theThirdController.Id = "13-qw-hg-45"

	runningInterface(theFirstController)
	runningInterface(theSecondController)
	runningInterface(theThirdController)

	theInterface.ExampleInterface2()
	theInterface.ErrorExample()
	theInterface.ErrorExample2()
}