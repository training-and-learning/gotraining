CREATE DATABASE `social-todo-db`;

USE `social-todo-db`;

CREATE TABLE `todo_items` (
`id` int NOT NULL AUTO_INCREMENT,
`title` varchar(150) DEFAULT NULL,
`description` text,
`image` json DEFAULT NULL,
`status` enum('Doing','Done','Deleted') DEFAULT 'Doing',
`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
`updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
KEY `todo_items_status_idx` (`status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `todo_items` VALUES(
    '1',
    'Title 1',
    'This is description 1',
    NULL,
    'Doing',
    NOW(),
    NOW()
);