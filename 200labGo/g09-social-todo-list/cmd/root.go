package cmd

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"social-todo-service/common"
	"social-todo-service/middleware"
	"social-todo-service/plugin/rpccaller"
	"social-todo-service/plugin/sdkgorm"
	"social-todo-service/plugin/simple"
	"social-todo-service/plugin/tokenprovider/jwt"
	"social-todo-service/pubsub"
	"social-todo-service/subcriber"

	"social-todo-service/module/upload"
	userStorage "social-todo-service/module/user/storage"

	gintransitem "social-todo-service/module/item/transport/gin_trans"
	gintransuser "social-todo-service/module/user/transport/gin_trans"
	gintransuserlikeitem "social-todo-service/module/userlikeitem/transport/gin_trans"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"gorm.io/gorm"
)

func newService() goservice.Service {
	service := goservice.New(
		goservice.WithName("social-todo-list"),
		goservice.WithVersion("1.0.0"),
		goservice.WithInitRunnable(sdkgorm.NewGormDB("main", common.PluginDBMain)),
		goservice.WithInitRunnable(jwt.NewTokenJWTProvider(common.PluginJWT)),
		goservice.WithInitRunnable(pubsub.NewPubSub(common.PluginPubSub)),
		goservice.WithInitRunnable(simple.NewSimplePlugin("sipmle")),
		goservice.WithInitRunnable(rpccaller.NewApiItemCaller(common.PluginItemAPI)),
	)

	return service
}

var rootCmd = &cobra.Command{
	Use:   "app",
	Short: "Start social TODO service",
	Run: func(cmd *cobra.Command, args []string) {

		service := newService()

		serviceLogger := service.Logger("serivce")

		if err := service.Init(); err != nil {
			serviceLogger.Fatalln(err)
		}

		service.HTTPServer().AddHandler(func(e *gin.Engine) {
			e.Use(middleware.Recover())

			// Example for Simple Plugin
			type CanGetValue interface {
				GetValue() string
			}
			log.Println(service.MustGet("simple").(CanGetValue).GetValue())
			/////////

			db := service.MustGet(common.PluginDBMain).(*gorm.DB)

			authStore := userStorage.NewSQLStore(db)
			middlewareAuth := middleware.RequireAuth(authStore, service)

			v1 := e.Group("/v1")
			{

				v1.PUT("/update", upload.Upload(service))

				v1.POST("/register", gintransuser.Register(service))
				v1.POST("/login", gintransuser.Login(service))
				v1.GET("/profile", middlewareAuth, gintransuser.Profile())

				items := v1.Group("/items", middlewareAuth)
				{
					items.POST("", gintransitem.CreateNewItem(service))
					items.GET("", gintransitem.GetListItem(service))
					items.GET("/:id", gintransitem.GetItem(service))
					items.PUT("")
					items.PATCH("/:id", gintransitem.UpdateItemData(service))
					items.DELETE("/:id", gintransitem.DeleteItemData(service))

					items.POST("/:id/like", gintransuserlikeitem.LikeItem(service))
					items.DELETE("/:id/like", gintransuserlikeitem.UnlikeItem(service))
					items.GET("/:id/like-users", gintransuserlikeitem.ListUserLiked(service))
				}

				rpc := v1.Group("rpc")
				{
					rpc.POST("/get_item_likes", gintransuserlikeitem.GetItemLikes(service))
				}

			}

			e.GET("/ping", func(c *gin.Context) {
				c.JSON(http.StatusOK, gin.H{
					"message": "pong",
				})
			})
		})

		subcriber.NewEngine(service).Start()

		if err := service.Start(); err != nil {
			serviceLogger.Fatalln(err)
		}
	},
}

func Execute() {
	rootCmd.AddCommand(outEnvCmd)
	rootCmd.AddCommand(cronDemoCmd)
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
