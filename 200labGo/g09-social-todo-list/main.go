package main

import (
	"encoding/json"
	"log"
	"time"

	"social-todo-service/cmd"
	"social-todo-service/common"
	"social-todo-service/module/item/model"
)

func main() {
	now := time.Now().UTC()
	item := model.TodoItem {
		SQLModel: common.SQLModel{
			Id: 1,
			CreatedAt: &now,
		},
		Title: "Task 1",
		Description: "Content 1",
		Status: "Doing",
		
	}

	jsData, err := json.Marshal(item)

	if err != nil {
		log.Fatalln(err)
	}

	log.Println(string(jsData))

	jsString := "{\"id\":1,\"title\":\"Task 1\",\"description\":\"Content 1\",\"status\":\"Doing\",\"created_at\":\"2023-01-11T04:12:25.0011165Z\"}"

	var item2  common.SQLModel

	if err := json.Unmarshal([]byte(jsString), &item2); err != nil {
		log.Fatalln(err)
	}

	log.Println(item2)
	///////////////
	
	cmd.Execute()

}

// func CreateItem(db *gorm.DB) func(ctx *gin.Context) {
// 	return func(ctx *gin.Context) {
// 		var itemData TodoItemCreation

// 		if err := ctx.ShouldBind(&itemData); err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})

// 			return
// 		}

// 		if err := db.Create(&itemData).Error; err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})

// 			return
// 		}

// 		ctx.JSON(http.StatusOK, gin.H{
// 			"data": itemData.Id,
// 		})
// 	}
// }

// func GetItem(db *gorm.DB) func(ctx *gin.Context) {
// 	return func(ctx *gin.Context) {
// 		var itemData TodoItem

// 		id, err := strconv.Atoi(ctx.Param("id"))

// 		if err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})
// 			return
// 		}

// 		if err := db.Where("id=?", id).First(&itemData).Error; err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})
// 			return
// 		}

// 		ctx.JSON(http.StatusOK, gin.H{
// 			"data":itemData,
// 		})
// 	}
// }

// func UpdateItem(db *gorm.DB) func(ctx *gin.Context){
// 	return func(ctx *gin.Context) {
// 		id, err := strconv.Atoi(ctx.Param("id"))
// 		if err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})
// 			return
// 		}

// 		var updateData TodoItemUpdate

// 		if err := db.Where("id=?", id).Updates(&updateData).Error; err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error":err.Error(),
// 			})
// 			return
// 		}

// 		ctx.JSON(http.StatusOK, gin.H{
// 			"data": true,
// 		})
// 	}
// }

// func DeleteItem(db *gorm.DB) func(ctx *gin.Context) {
// 	return func(ctx *gin.Context) {
// 		id, err := strconv.Atoi(ctx.Param("id"))

// 		if err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error":err.Error(),
// 			})
// 			return
// 		}

// 		/**
// 		May u will ask why we should call the Table() Method in db at delete func,
// 		why in create, get, update, method it doesn't appear ?
// 		Because in create, get ,update we pass the pointer ItemObject ex: TodoItem, TodoItemCreation, TodoItemUpdate
// 		and in those pointer there're already had a pointer which already have a table name for it 
// 		And in Delete method we just pass a id, we don't have a pointer/object so we must indicate the table name for it to delete
// 		*/
// 		/**
// 			There're 2 way to update
// 			this way will not require table name because we pass into function a pointer/object
// 			var deleteItem := "Deleted"
// 			db.Where("id=?",id).Updates(&TodoItemUpdate{Status:&deleteItem}).Error; .....
// 		*/
// 		if err := db.Table(TodoItem{}.TableName()).Where("id=?",id).Updates(map[string]interface{}{
// 			"status":"Deleted",
// 		}).Error; err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})

// 			return
// 		}

// 		ctx.JSON(http.StatusOK, gin.H{
// 			"data": true,
// 		})
// 	}
// }

// func ListItem(db *gorm.DB) func(ctx *gin.Context) {
// 	return func(ctx *gin.Context) {
// 		var paging common.Paging

// 		if err := ctx.ShouldBind(&paging); err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})

// 			return
// 		}

// 		paging.Process()

// 		var result []TodoItem

// 		db = db.Table(TodoItem{}.TableName()).Where("status <> ?","Deleted")

// 		if err := db.Select("id").Count(&paging.Total).Error; err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error": err.Error(),
// 			})
// 			return
// 		}

// 		if err := db .Select("*").Offset(( paging.Page-1 ) * paging.Limit).Limit(paging.Limit).Order("id desc").Find(&result).Error; err != nil {
// 			ctx.JSON(http.StatusBadRequest, gin.H{
// 				"error":err.Error(),
// 			})
// 			return
// 		}

// 		ctx.JSON(http.StatusOK, gin.H {
// 			"data": result,
// 			"paging": paging,
// 		})
// 	}
// }