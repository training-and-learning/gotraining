package middleware

import (
	"context"
	"errors"
	"social-todo-service/common"
	"social-todo-service/module/user/model"
	"social-todo-service/plugin/tokenprovider"
	"strings"

	goservice "github.com/200Lab-Education/go-sdk"

	"github.com/gin-gonic/gin"
)

type AuthenSore interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error)
}

func ErrWrongAuthHeader(err error) *common.AppError {
	return common.NewCustomError(
		err,
		"wrong authen header",
		"ErrWrongAuthHeader",
	)
}

func extractTokenFromHeaderString(s string) (string, error) {
	parts := strings.Split(s, " ")
	if parts[0] != "Bearer" || len(parts) < 2 || strings.TrimSpace(parts[1]) == "" {
		return "", ErrWrongAuthHeader(nil)
	}
	return parts[1], nil
}

func RequireAuth(authStore AuthenSore, serviceCtx goservice.ServiceContext) func (c *gin.Context) {
	return func(c *gin.Context) {
		token, err := extractTokenFromHeaderString(c.GetHeader("Authorization"))

		if err != nil {
			panic(err)
		}
		tokenProvider := serviceCtx.MustGet(common.PluginJWT).(tokenprovider.Provider)
		payload, err := tokenProvider.Validate(token)

		if err != nil {
			panic(err)
		}

		user, err := authStore.FindUser(c.Request.Context(), map[string]interface{}{"id": payload.UserId()})

		if err != nil {
			panic(err)
		}

		if user.Status == 0 {
			panic(common.ErrNoPermission(errors.New("user has been deleted or banned")))
		}

		c.Set(common.CurrentUser, user)
		c.Next()
	}
}