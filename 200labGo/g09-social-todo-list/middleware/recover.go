package middleware

import (
	"social-todo-service/common"

	"github.com/gin-gonic/gin"
)

func Recover() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		defer func ()  {
			if err := recover(); err != nil {
				ctx.Header("Content-type", "application/json")
				if appErr, ok := err.(*common.AppError); ok {
					ctx.AbortWithStatusJSON(appErr.StatusCode, appErr)
					panic(err)
				}

				appErr := common.ErrInternal(err.(error))
				ctx.AbortWithStatusJSON(appErr.StatusCode, appErr)
				panic(appErr)
			}
		}()
		ctx.Next()
	}
}