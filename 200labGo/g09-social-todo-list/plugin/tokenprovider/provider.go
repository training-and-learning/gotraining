package tokenprovider

import (
	"errors"
	"social-todo-service/common"
)

type Provider interface {
	Generate(data common.TokenPayLoad, expiry int) (Token, error)
	Validate(token string) (TokenPayLoad, error)
	SecretKey() string
}

type TokenPayLoad interface {
	UserId() int
	Role() string
}

type Token interface {
	GetToken() string
}

var (
	ErrNotFound = common.NewCustomError(
		errors.New("token not found"),
		"token not found",
		"ErrNotFound",
	)

	ErrEncodeToken = common.NewCustomError(
		errors.New("error encoding the toekn"),
		"error encoding the token",
		"ErrEncodingToken",
	)

	ErrInvalidToken = common.NewCustomError(
		errors.New("invalid token provided"),
		"invalid token provided",
		"ErrInvalidToken",
	)
)