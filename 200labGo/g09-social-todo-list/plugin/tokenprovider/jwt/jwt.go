package jwt

import (
	"flag"
	"fmt"
	"social-todo-service/common"
	"social-todo-service/plugin/tokenprovider"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type jwtProvider struct {
	name string
	secret string
}

func NewTokenJWTProvider(name string) *jwtProvider {
	return &jwtProvider{name: name}
}

func (p *jwtProvider) GetPrefix() string {
	return p.Name()
}

func (p *jwtProvider) Get() interface{} {
	return p
}

func (p *jwtProvider) Name() string {
	return p.name
}

func (p *jwtProvider) InitFlags() {
	flag.StringVar(&p.secret, "jwt-secret", "200Lab.io", "Secret key for generating JWT")
}

func (p *jwtProvider) Configure() error {
	return nil
}

func (p *jwtProvider) Run() error {
	return nil
}

func (p *jwtProvider) Stop() <-chan bool {
	c := make(chan bool)
	go func() {
		c <- true
	}()
	return c
}

func (j *jwtProvider) SecretKey() string {
	return j.secret
}

type myClaims struct {
	PayLoad common.TokenPayLoad `json:"payload"`
	jwt.RegisteredClaims
}

type token struct {
	Token string `json:"token"`
	Created time.Time `json:"created"`
	Expiry int `json:"expiry"`
}

func (t *token) GetToken() string {
	return t.Token
}

func (j *jwtProvider) Generate(data common.TokenPayLoad, expiry int) (tokenprovider.Token, error) {
	now := time.Now()

	t := jwt.NewWithClaims(jwt.SigningMethodHS256, myClaims{
		common.TokenPayLoad{
			UId: data.UserId(),
			URole: data.Role(),
		},
		jwt.RegisteredClaims{
			ExpiresAt: &jwt.NumericDate{
				Time: now.Local().Add(time.Second * time.Duration(expiry)),
			},
			IssuedAt: &jwt.NumericDate{
				Time: now.Local(),
			},
			ID: fmt.Sprintf("%d", now.UnixNano()),
		},
	})

	myToken, err := t.SignedString([]byte(j.secret))
	if err != nil {
		return nil, err
	}

	return &token{
		Token: myToken,
		Expiry: expiry,
		Created: now,
	}, nil
}

func (j *jwtProvider) Validate(myToken string) (tokenprovider.TokenPayLoad, error) {
	res, err := jwt.ParseWithClaims(myToken, &myClaims{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(j.secret), nil
	})

	if err != nil {
		return nil, tokenprovider.ErrInvalidToken
	}

	if !res.Valid {
		return nil, tokenprovider.ErrInvalidToken
	}

	claims, ok := res.Claims.(*myClaims)

	if !ok {
		return nil, tokenprovider.ErrInvalidToken
	}

	return claims.PayLoad, nil
}