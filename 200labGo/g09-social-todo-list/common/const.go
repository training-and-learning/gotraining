package common

import "fmt"

type DbType int

const (
	CurrentUser          = "current_user"
	PluginDBMain         = "mysql"
	PluginJWT            = "jwt"
	PluginPubSub         = "pubsub"
	PluginItemAPI        = "item-api"
	DbTypeItem    DbType = 1
	DbTypeUser    DbType = 2

	TopicUserLikedItem   = "TopicUserLikedItem"
	TopicUserUnlikedItem = "TopicUserUnlikedItem"
)

func Recovery() {
	if r := recover(); r != nil {
		fmt.Println("Recovered: ", r)
	}
}

type TokenPayLoad struct {
	UId   int    `json:"user_id"`
	URole string `json:"role"`
}

func (p TokenPayLoad) UserId() int {
	return p.UId
}

func (p TokenPayLoad) Role() string {
	return p.URole
}

type Requester interface {
	GetUserId() int
	GetEmail() string
	GetRole() string
}

func IsAdmin(requester Requester) bool {
	return requester.GetRole() == "admin" || requester.GetRole() == "mod"
}
