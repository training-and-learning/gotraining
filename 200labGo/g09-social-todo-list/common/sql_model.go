package common

import "time"

type SQLModel struct {
	Id        int `json:"-" gorm:"column:id;"`
	FakeId *UID `json:"id" gorm:"-"`
	/**
        should use a pointer because when you do not use it, 
        the time of creation at when it null it will be default 1 Jan 1937 regardless in the database it null
    */
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at;"`
	/**
		omitempty is when you not init the field, it will be get rid of that field when convert to json 
	*/
	UpdatedAt *time.Time `json:"updated_at,omitempty" gorm:"column:updated_at;"`
}

func (sqlModel *SQLModel) Mask(dbType DbType) {
	uid := NewUID(uint32(sqlModel.Id), int(dbType), 1)
	sqlModel.FakeId = &uid
}