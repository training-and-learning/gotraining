### Build Docker-Compose with .env file

```bash
docker-compose -f <path_filename> --env-file <path_env_filename> up -d
```
