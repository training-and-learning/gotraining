package gintrans

import (
	"net/http"
	"social-todo-service/common"

	//itemStorage "social-todo-service/module/item/storage"
	"social-todo-service/module/userlikeitem/biz"
	"social-todo-service/module/userlikeitem/storage"
	"social-todo-service/pubsub"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func UnlikeItem(serviceCtx goservice.ServiceContext) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := common.FromBase58(ctx.Param("id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := ctx.MustGet(common.CurrentUser).(common.Requester)

		db := serviceCtx.MustGet(common.PluginDBMain).(*gorm.DB)
		ps := serviceCtx.MustGet(common.PluginPubSub).(pubsub.PubSub)
		store := storage.NewSQLStore(db)
		//itemStorage := itemStorage.NewSQLStore(db)
		business := biz.NewUserUnlikeItemBiz(store, ps)

		if err := business.UnlikeItem(
			ctx.Request.Context(), 
			requester.GetUserId(),
			int(id.GetLocalID()),
		); err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(true))
	}
}