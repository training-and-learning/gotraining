package gintrans

import (
	"net/http"
	"social-todo-service/common"
	"social-todo-service/module/userlikeitem/biz"
	"social-todo-service/module/userlikeitem/storage"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func ListUserLiked(serviceCtx goservice.ServiceContext) gin.HandlerFunc {
	 return func(ctx *gin.Context) {
		id, err := common.FromBase58(ctx.Param("id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		var queryString struct {
			common.Paging
		}

		if err := ctx.ShouldBind(&queryString); err != nil {
			panic(err)
		}

		queryString.Process()

		db := serviceCtx.MustGet(common.PluginDBMain).(*gorm.DB)
		store := storage.NewSQLStore(db)
		business := biz.NewListUserLikeItemBiz(store)

		result ,err := business.ListUserLikedItem(ctx.Request.Context(), int(id.GetLocalID()), &queryString.Paging)

		if err != nil {
			panic(err)
		}

		for i := range result {
			result[i].Mask()
		}

		ctx.JSON(http.StatusOK, common.NewSuccessResponse(result, queryString.Paging, nil))
	 }
}