package gintrans

import (
	"net/http"
	"social-todo-service/common"

	//itemStorage "social-todo-service/module/item/storage"
	"social-todo-service/module/userlikeitem/biz"
	"social-todo-service/module/userlikeitem/model"
	"social-todo-service/module/userlikeitem/storage"
	"social-todo-service/pubsub"
	"time"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func LikeItem(service goservice.ServiceContext) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		id, err := common.FromBase58(ctx.Param("id"))

		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := ctx.MustGet(common.CurrentUser).(common.Requester)
		db := service.MustGet(common.PluginDBMain).(*gorm.DB)
		ps := service.MustGet(common.PluginPubSub).(pubsub.PubSub)
		store := storage.NewSQLStore(db)
		//itemStorage := itemStorage.NewSQLStore(db)
		business := biz.NewUserLikeItemBiz(store, ps)
		now := time.Now().UTC()

		if err := business.LikeItem(ctx.Request.Context(), &model.Like{
			UserId: requester.GetUserId(),
			ItemId: int(id.GetLocalID()),
			CreatedAt: &now,
		}); err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(true))

	}
}