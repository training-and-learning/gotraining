package gintrans

import (
	"net/http"
	"social-todo-service/common"
	"social-todo-service/module/userlikeitem/storage"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func GetItemLikes(serviceCtx goservice.ServiceContext) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		type RequestData struct {
			Ids []int `json:"ids"`
		}

		var data RequestData

		if err := ctx.ShouldBind(&data); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}

		db := serviceCtx.MustGet(common.PluginDBMain).(*gorm.DB)
		store := storage.NewSQLStore(db)
		mapRs, err := store.GetItemLikes(ctx.Request.Context(), data.Ids)
		if err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(mapRs))
	}
}
