package biz

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/userlikeitem/model"
)

type ListUserLikedItemStore interface {
	ListUsers(
		ctx context.Context,
		itemId int,
		paging *common.Paging,
	) ([]common.SimpleUser, error)
}

type listUserLikedItemBiz struct {
	store ListUserLikedItemStore
}

func NewListUserLikeItemBiz(store ListUserLikedItemStore) *listUserLikedItemBiz {
	return &listUserLikedItemBiz{store: store}
}

func (biz *listUserLikedItemBiz) ListUserLikedItem(
	ctx context.Context, 
	itemId int,
	paging *common.Paging,
) ([]common.SimpleUser, error) {
	result, err := biz.store.ListUsers(ctx, itemId, paging)

	if err != nil {
		return nil, common.ErrCannotListEntity(model.EntityName, err)
	}

	return result, nil
}