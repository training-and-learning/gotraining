package biz

import (
	"context"
	"log"
	"social-todo-service/common"
	"social-todo-service/module/userlikeitem/model"
	"social-todo-service/pubsub"
)

type UserLikeItemStore interface {
	Create(ctx context.Context, data *model.Like) error
}

// type IncreaseItemStorage interface {
// 	IncreaseLikeCount(ctx context.Context, id int) error
// }

type userLikeItemBiz struct {
	store UserLikeItemStore
	ps pubsub.PubSub
	//itemStore IncreaseItemStorage
}

func NewUserLikeItemBiz(
	store UserLikeItemStore, 
	ps pubsub.PubSub,
	) *userLikeItemBiz {
	return &userLikeItemBiz{store:store, ps: ps}
}

func (biz *userLikeItemBiz) LikeItem(ctx context.Context, data *model.Like) error {
	if err := biz.store.Create(ctx, data); err != nil {
		return model.ErrCannotLikeItem(err)
	}
	if err := biz.ps.Publish(ctx, common.TopicUserLikedItem,
	pubsub.NewMessage(data)); err != nil {
		log.Println(err)
	}
	// go func() {
	// 	defer common.Recovery()

	// 	if err := biz.itemStore.
	// 		IncreaseLikeCount(ctx, data.ItemId); err != nil {
	// 			log.Println(err)
	// 		} 
	// }()
	return nil
}