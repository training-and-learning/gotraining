package upload

import (
	"fmt"
	"net/http"
	"os"
	"social-todo-service/common"
	"time"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
)

func Upload(serviceCtx goservice.ServiceContext) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		
		fileHeader, err := ctx.FormFile("file")

		if err != nil {
			panic(common.ErrInternal(err))
		}

		dst := fmt.Sprintf("static/%d.%s", time.Now().UTC().UnixNano(), fileHeader.Filename)

		if err := ctx.SaveUploadedFile(fileHeader, dst); err != nil {

		}

		image := common.Image {
			Id: 0,
			Url: dst,
			Width: 100,
			Height: 100,
			CloudName: "local",
			Extension: "",
		}

		image.FulFill(fmt.Sprintf("http://localhost:%s", os.Getenv("SERVICE_PORT")))

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(image))
	}
}