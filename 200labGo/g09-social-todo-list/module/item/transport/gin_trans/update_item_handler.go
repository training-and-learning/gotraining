package gintrans

import (
	"net/http"
	"social-todo-service/common"
	"social-todo-service/module/item/biz"
	"social-todo-service/module/item/model"
	"social-todo-service/module/item/storage"
	"strconv"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func UpdateItemData(serviceCtx goservice.ServiceContext) func(*gin.Context) {
	return func(ctx *gin.Context) {
		db := serviceCtx.MustGet(common.PluginDBMain).(*gorm.DB)
		var dataReq model.TodoItemUpdate
		
		id,err := strconv.Atoi(ctx.Param("id"))
			
		if err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		if err := ctx.ShouldBind(&dataReq); err != nil {
			panic(common.ErrInvalidRequest(err))
		}
		requester := ctx.MustGet(common.CurrentUser).(common.Requester)
		store := storage.NewSQLStore(db)
		business := biz.NewUpdateItemBiz(store, requester)

		if err := business.UpdateItemData(ctx.Request.Context(), id, &dataReq); err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(id))
	}
}