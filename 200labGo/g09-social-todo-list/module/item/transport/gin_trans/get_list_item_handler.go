package gintrans

import (
	"net/http"
	"social-todo-service/common"
	"social-todo-service/module/item/biz"
	"social-todo-service/module/item/model"
	"social-todo-service/module/item/repository"
	"social-todo-service/module/item/storage"
	"social-todo-service/module/item/storage/restapi"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func GetListItem(serviceCtx goservice.ServiceContext) func(*gin.Context) {
	return func(ctx *gin.Context) {
		db := serviceCtx.MustGet(common.PluginDBMain).(*gorm.DB)
		apiItemCaller := serviceCtx.MustGet(common.PluginItemAPI).(interface {
			GetServiceURL() string
		})
		var queryString struct {
			model.Filter
			common.Paging
		}

		if err := ctx.ShouldBind(&queryString); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		queryString.Paging.Process()

		requester := ctx.MustGet(common.CurrentUser).(common.Requester)

		store := storage.NewSQLStore(db)
		likeStore := restapi.New(apiItemCaller.GetServiceURL(), serviceCtx.Logger("restapi.itemlikes"))
		repo := repository.NewListItemRepo(store, likeStore, requester)
		business := biz.NewGetListItemBiz(repo, requester)
		data, err := business.GetListItemData(ctx, &queryString.Filter, &queryString.Paging)
		if err != nil {
			panic(err)
		}
		for i := range data {
			data[i].Mask()
		}
		ctx.JSON(http.StatusOK, common.NewSuccessResponse(data, queryString.Paging, queryString.Filter))
	}
}
