package gintrans

import (
	"net/http"
	"social-todo-service/common"
	"social-todo-service/module/item/biz"
	"social-todo-service/module/item/model"
	"social-todo-service/module/item/storage"

	goservice "github.com/200Lab-Education/go-sdk"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func CreateNewItem(serviceCtx goservice.ServiceContext) func(*gin.Context) {
	return func(ctx *gin.Context) {
		db := serviceCtx.MustGet(common.PluginDBMain).(*gorm.DB)
		var dataRes model.TodoItemCreation
		if err := ctx.ShouldBind(&dataRes); err != nil {
			panic(common.ErrInvalidRequest(err))
		}

		requester := ctx.MustGet(common.CurrentUser).(common.Requester)
		dataRes.UserId = requester.GetUserId()

		store := storage.NewSQLStore(db)
		business := biz.NewCreateItemBiz(store)

		if err := business.NewItem(ctx.Request.Context(), &dataRes); err != nil {
			panic(err)
		}

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(dataRes.Id))
	}
}