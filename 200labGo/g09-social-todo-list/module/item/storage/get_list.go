package storage

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/item/model"
)

func (s *sqlStore) GetListItem(
	ctx context.Context, 
	filter *model.Filter, 
	paging *common.Paging, 
	moreKeys ...string) ([]model.TodoItem, error) {
	var result []model.TodoItem

	db := s.db.Table(model.TodoItem{}.TableName()).Where("status <> ?","Deleted")


	// Get items of requester only
	//requester := ctx.Value(common.CurrentUser).(common.Requester)
	//db = db.Where("user_id = ?", requester.GetUserId())

	if f := filter; f != nil {
		if v := f.Status; v != "" {
			db = db.Where("status = ?", v)
		}
	}

	if err := db.
		Select("id").
		Count(&paging.Total).Error; err != nil {
		return nil, err	
	}

	for i := range moreKeys {
		db = db.Preload(moreKeys[i])
	}

	if v := paging.FakeCursor; v != "" {
		uid, err := common.FromBase58(v)

		if err != nil {
			return nil, common.ErrDB(err)
		}

		db = db.Where("id < ?", uid.GetLocalID())
	} else {
		db = db.Offset((paging.Page-1)*paging.Limit)
	}

	if err := db.
		Select("*").
		Limit(paging.Limit).
		Order("id desc").
		Find(&result).Error; err != nil {
		return nil, common.ErrDB(err)
	}

	if len(result) > 0 {
		result[len(result)-1].Mask()
		paging.NextCursor = result[len(result)-1].FakeId.String()
	}
	return result, nil
}