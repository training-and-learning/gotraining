package storage

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/item/model"
)

func (s *sqlStore) DeleteItem(ctx context.Context, cond map[string]interface{}) error {
	/**
		May u will ask why we should call the Table() Method in db at delete func,
		why in create, get, update, method it doesn't appear ?
		Because in create, get ,update we pass the pointer ItemObject ex: TodoItem, TodoItemCreation, TodoItemUpdate
		and in those pointer there're already had a pointer which already have a table name for it 
		And in Delete method we just pass a id, we don't have a pointer/object so we must indicate the table name for it to delete
		*/
		/**
			There're 2 way to update
			this way will not require table name because we pass into function a pointer/object
			var deleteItem := "Deleted"
			db.Where("id=?",id).Updates(&TodoItemUpdate{Status:&deleteItem}).Error; .....
		*/
	if err := s.db.Table(model.TodoItem{}.TableName()).Where(cond).Updates(map[string]interface{}{"status":"Deleted"}).Error; err != nil {
		return common.ErrDB(err)
	}
	return nil
}