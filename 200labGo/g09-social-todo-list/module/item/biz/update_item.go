package biz

import (
	"context"
	"errors"
	"social-todo-service/common"
	"social-todo-service/module/item/model"
)

type UpdateItemStore interface {
	GetItem(ctx context.Context, cond map[string]interface{}) (*model.TodoItem, error)
	UpdateItem(ctx context.Context, cond map[string]interface{}, dataUpdate *model.TodoItemUpdate) error 
}

type updateItemBiz struct {
	store UpdateItemStore
	requester common.Requester
}

func NewUpdateItemBiz(store UpdateItemStore, requester common.Requester) *updateItemBiz {
	return &updateItemBiz{store: store, requester: requester}
}

func (biz *updateItemBiz) UpdateItemData(ctx context.Context, id int, data *model.TodoItemUpdate) error {
	
	item, err := biz.store.GetItem(ctx, map[string]interface{}{"id":id})

	if err != nil {
		return common.ErrCannotGetEntity(model.EntityName, err)
	}

	if item.Status == "Deleted" {
		return model.ErrItemIsDeleted
	}

	isOwner := biz.requester.GetUserId() == item.UserId

	if !isOwner && !common.IsAdmin(biz.requester) {
		return common.ErrNoPermission(errors.New("no permission"))
	}

	if err := biz.store.UpdateItem(ctx, map[string]interface{}{"id":id}, data); err != nil {
		return common.ErrCannotUpdateEntity(model.EntityName, err)
	}
	return nil
}