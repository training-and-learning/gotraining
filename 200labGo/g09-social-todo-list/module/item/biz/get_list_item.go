package biz

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/item/model"
)

type ListItemRepo interface {
	ListItem(ctx context.Context, 
		filter *model.Filter, 
		paging *common.Paging, 
		moreKeys ...string) ([]model.TodoItem, error)
}

type getListItemBiz struct {
	repo ListItemRepo
	requester common.Requester
}

func NewGetListItemBiz(repo ListItemRepo, requester common.Requester) *getListItemBiz{
	return &getListItemBiz{repo: repo, requester: requester}
}


func (biz *getListItemBiz) GetListItemData(ctx context.Context, filter *model.Filter, paging *common.Paging) ([]model.TodoItem, error) {
	ctxStore := context.WithValue(ctx, common.CurrentUser, biz.requester)
	data, err := biz.repo.ListItem(ctxStore, filter, paging, "Owner")

	if err != nil {
		return nil, common.ErrCannotListEntity(model.EntityName, err)
	}
	return data, nil
}