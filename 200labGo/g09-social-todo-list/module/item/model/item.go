package model

import (
	"errors"
	"social-todo-service/common"
	"strings"
)

var(
	ErrTitleCannotBeEmpty = errors.New("title cannot be empty")
	ErrItemIsDeleted = errors.New("item is deleted")
)

const (
	EntityName = "Item"
)

type TodoItem struct {
	/**
	This is Struct Embedding 
	*/
	common.SQLModel 
	UserId int `json:"-" gorm:"column:user_id;"`
	Title       string `json:"title" gorm:"column:title;"`
	Description string `json:"description" gorm:"column:description;"`
	Status      string `json:"status" gorm:"column:status;"`
	Image *common.Image `json:"image" gorm:"column:image;"`
	LikeCount int `json:"liked_count" gorm:"-"`
	Owner *common.SimpleUser `json:"owner" gorm:"foreignKey:UserId;"`
}

func (TodoItem) TableName() string { return "todo_items" }

func (i *TodoItem) Mask() {
	i.SQLModel.Mask(common.DbTypeItem)
	if v := i.Owner; v != nil {
		v.Mask()
	}
}

type TodoItemCreation struct {
	Id          int    `json:"id" gorm:"column:id;"`
	UserId int `json:"user_id" gorm:"column:user_id;"`
	Title       string `json:"title" gorm:"column:title;"`
	Description string `json:"description" gorm:"column:description;"`
	Image *common.Image `json:"image" gorm:"column:image;"`
}

func (TodoItemCreation) TableName() string { return TodoItem{}.TableName() }

func (ic *TodoItemCreation) Validate() error {
	ic.Title = strings.TrimSpace(ic.Title)
	if ic.Title == "" {
		return ErrTitleCannotBeEmpty
	}
	return nil
}  


type TodoItemUpdate struct {
	/**
	GORM when it met empty string "", boolen false , int 0, pointer nil it will ignore and not update
	so if you want to update like an empty string to db you must declare like the below example:
	Title *string -> for update empty string to db
	Title string -> not update to db if you pass the empty string
	So the question is what if you want to update a pointer which have a value nil to db ???
	What should you dotime
	This is the right answer https://stackoverflow.com/questions/63852322/gorm-updating-null-field-when-calling-updates

	or just like this
	db.Where("id=?",id).Updates(map[string]interface{}{ "title": nil }).Error; ..... => this will update nil value to db
	*/
	Title       *string `json:"title" gorm:"column:title;"`
	Description *string `json:"description" gorm:"column:description;"`
	Status      *string `json:"status" gorm:"column:status;"`
}

func (TodoItemUpdate) TableName() string { return TodoItem{}.TableName() }