package gintrans

import (
	"net/http"
	"social-todo-service/common"

	"github.com/gin-gonic/gin"
)

func Profile() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		u := ctx.MustGet(common.CurrentUser)

		ctx.JSON(http.StatusOK, common.SimpleSuccessResponse(u))
	}
}