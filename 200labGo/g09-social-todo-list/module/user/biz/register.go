package biz

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/user/model"
)

type RegisterStorage interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error)
	CreateUser(ctx context.Context, data *model.UserCreate) error
}

type Hasher interface {
	Hash(data string) string
}

type registerBusiness struct {
	registerStore RegisterStorage
	hasher Hasher
}

func NewRegisterBusiness(
	registerStorage RegisterStorage,
	hasher Hasher,
) *registerBusiness {
	return &registerBusiness{
		registerStore: registerStorage,
		hasher: hasher,
	}
}

func (business *registerBusiness) Register(ctx context.Context, data *model.UserCreate) error {
	user, _ := business.registerStore.FindUser(ctx, map[string]interface{}{"email": data.Email})
	if user != nil {
		return model.ErrEmailExisted
	}

	salt := common.GenSalt(50)

	data.Password = business.hasher.Hash(data.Password + salt)
	data.Salt = salt
	data.Role = "user"

	if err := business.registerStore.CreateUser(ctx, data); err != nil {
		return common.ErrCannotCreateEntity(model.EntityName, err)
	}
	return nil
}