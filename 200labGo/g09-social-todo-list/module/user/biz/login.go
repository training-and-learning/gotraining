package biz

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/user/model"
	"social-todo-service/plugin/tokenprovider"
)

type LoginStorage interface {
	FindUser(ctx context.Context, conditions map[string]interface{}, moreInfo ...string) (*model.User, error)
}

type loginBusiness struct {
	storeUser LoginStorage
	tokenProvider tokenprovider.Provider
	hasher Hasher
	expiry int
}

func NewLoginBusiness(
	storageUser LoginStorage, 
	tokenProvider tokenprovider.Provider,
	hasher Hasher, 
	expiry int,
) *loginBusiness {
	return &loginBusiness{
		storeUser: storageUser,
		tokenProvider: tokenProvider,
		hasher: hasher,
		expiry: expiry,
	}
}

func (business *loginBusiness) Login(ctx context.Context, data *model.UserLogin) (tokenprovider.Token, error) {
	user, err := business.storeUser.FindUser(ctx, map[string]interface{}{"email":data.Email})

	if err != nil {
		return nil, model.ErrEmailOrPasswordInvalid
	}

	passHashed := business.hasher.Hash(data.Password + user.Salt)

	if user.Password != passHashed {
		return nil, model.ErrEmailOrPasswordInvalid
	}

	payload := &common.TokenPayLoad {
		UId: user.Id,
		URole: user.Role.String(),
	}

	accessToken, err := business.tokenProvider.Generate(*payload, business.expiry)
	if err != nil {
		return nil, common.ErrInternal(err)
	}

	return accessToken, nil
}