#!/usr/bin/bash

IMAGE_NAME=social-todo-service
CACHED_BUILD=$1

if [[ -n "$CACHED_BUILD" ]]; then
  echo "Docker building cached image..."
  docker rmi ${IMAGE_NAME}-cached ${IMAGE_NAME}
  docker build -t ${IMAGE_NAME}-cached -f DockerFileCache .
fi
echo "DONE"