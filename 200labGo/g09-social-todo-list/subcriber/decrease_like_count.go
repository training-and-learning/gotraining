package subcriber

import (
	"context"
	"social-todo-service/common"
	"social-todo-service/module/item/storage"
	"social-todo-service/pubsub"

	goservice "github.com/200Lab-Education/go-sdk"
	"gorm.io/gorm"
)

type HasItemId interface {
	GetItemId() int
}

func DecreaseLikeCountAfterUserUnlikeItem(serverCtx goservice.ServiceContext) subJob {
	return subJob{
		Title: "Decrease like count after user unlikes item",
		Hld:   func(ctx context.Context, message *pubsub.Message) error {
			db := serverCtx.MustGet(common.PluginDBMain).(*gorm.DB)
			data := message.Data().(HasItemId)
			return storage.NewSQLStore(db).DecreaseLikeCount(ctx, data.GetItemId())
		},
	}
}