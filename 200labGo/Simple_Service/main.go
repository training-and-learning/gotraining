package main

import (
	"fmt"
	"log"
	"os"

	treeEntity "simple-service/entity"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)
  
  func main() {
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	err := godotenv.Load("local.env")
	if err != nil {
		log.Fatal(err)
	}
	dsn := 
	fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
	os.Getenv("DB_HOST"),
	os.Getenv("DB_PASSWORD"),
	os.Getenv("DB_IP"),
	os.Getenv("DB_PORT"),
	os.Getenv("DB_NAME"))
	
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	fmt.Println(db, err)

	/**
	insert
	*/
	// newTreeType := treeEntity.TreeType{
	// 	Name: "Tree type go loai 1",
	// 	Description: "Day la tree type go loai 1",
	// }

	// db.Create(newTreeType)

	/**
	select
	*/
	var listTreeType []treeEntity.TreeType
	// db.Where("Name LIKE '%?%'","bonsai").Find(listTreeType)
	// fmt.Printf("%v\n", listTreeType)
	db.Table(treeEntity.TreeType{}.TableName()).Find(&listTreeType)
	fmt.Printf("%v\n", listTreeType)
  }