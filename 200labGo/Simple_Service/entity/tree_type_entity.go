package tree_type_entity

import (
	"time"
)

type TreeType struct {
	UUID        string `json:"uuid_tree_type" gorm:"column:UUID_TreeType"`
	Name        string `json:"name" gorm:"column:Name"`
	Description  string `json:"description" gorm:"column:Description"`
	CreatedDate time.Time `json:"created_date" gorm:"column:CreateDate"`
	UpdateDate  time.Time `json:"updated_date" gorm:"column:UpdateDate"`
}

func (TreeType) TableName() string{
	return "tree_type"
}