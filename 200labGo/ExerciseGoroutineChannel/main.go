package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	listUrl := make([]int, 0)
	for i := 0; i < 20; i++ {
		listUrl = append(listUrl, rand.Intn(1000))
	}
	fmt.Printf("%v\n", listUrl)
	channelUrl := generatePipeUrl(listUrl)
	waitGroup := new(sync.WaitGroup)
	waitGroup.Add(2)
	/**
		Cách 1

	*/
	// channel1 := fanout(channelUrl)
	// channel2 := fanout(channelUrl)
	// go func() {
	// 	for value := range channel1 {
	// 		fmt.Printf("Url in channel 1: %v \n", value)
	// 	}
	// 	waitGroup.Done()
	// }()
	// go func() {
	// 	for value := range channel2 {
	// 		fmt.Printf("Url in channel 2: %v \n", value)
	// 	}
	// 	waitGroup.Done()
	// }()

	/**
		Cách 2
	*/
	go func() {
		for url := range channelUrl {
			fmt.Printf("Url in goroutine 1: %v \n", url)
			time.Sleep(time.Second*1)
		}
		waitGroup.Done()
	}()
	go func() {
		for url := range channelUrl {
			fmt.Printf("Url in goroutine 2: %v \n", url)
			time.Sleep(time.Second*1)
		}
		waitGroup.Done()
	}()
	

	waitGroup.Wait()
}

func generatePipeUrl(listUrl []int) <-chan int{
	out := make(chan int)
	go func() {
		for _,value := range listUrl {
			out <- value
		}
		close(out)
	}()
	return out
}

func fanout(fanin <-chan int) <-chan int{
	out := make(chan int)
	go func() {
		for url := range fanin {
			out <- url
		}
		close(out)
	}()
	return out
}