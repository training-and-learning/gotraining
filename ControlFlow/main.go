package main

import (
	theFlow "app/src"
)

var (
	coins = 50
	users = []string{
		"Matthew", "Sarah", "Augustus", "Heidi", "Emilie",
		"Peter", "Giana", "Adriano", "Aaron", "Elizabeth",
	}
)

func main() {
	theFlow.ExcerciseFlow(coins, users)
}