package theFlow

import (
	"fmt"
	"strings"
)

func ExcerciseFlow(coins int, users []string) {
	distribution := make(map[string]int, len(users))

	for _,value := range users {
		distribution[value] = 0
		temp := strings.ToLower(value)
		for i := 0; i < len(temp); i++ {
			switch temp[i] {
			case 'a','e':
				if distribution[value] += 1; distribution[value] > 10 {
					distribution[value] = 10
				} 
			case 'i':
				if distribution[value] += 2; distribution[value] > 10 {
					distribution[value] = 10
				} 
			case 'o':
				if distribution[value] += 3; distribution[value] > 10 {
					distribution[value] = 10
				} 
			case 'u':
				if distribution[value] += 4; distribution[value] > 10 {
					distribution[value] = 10
				}
			}
		}
		coins-=distribution[value]
	}

	fmt.Println(distribution)
	fmt.Println("Coins left:", coins)
}