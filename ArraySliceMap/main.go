package main

import (
	// theArray "app/array"
	// theMap "app/map"
	// theSlices "app/slices"
	"fmt"
	"sort"
	"strings"
	//"sync"
)

func main() {
	// theArray.ArrayExample()
	// theSlices.SlicesExample()
	// theMap.MapExample()
	// theArray.ArrayExample2()
	// Exercise()
	fmt.Println(trap([]int{}))
}


func Exercise(){
	var names = []string{"Katrina", "Evan", "Neil", "Adam", "Martin", "Matt",
	"Emma", "Isabella", "Emily", "Madison",
	"Ava", "Olivia", "Sophia", "Abigail",
	"Elizabeth", "Chloe", "Samantha",
	"Addison", "Natalie", "Mia", "Alexis"}
	theNameMap := make(map[string]int)
	var maxLength int = 0
	for _,value := range names{
		length := len(value)
		theNameMap[value] = length
		if maxLength < length {
			maxLength = length
		}
	}
	theSlicesResult := make([][]string, maxLength+1)
	for key,value := range theNameMap{
		theSlicesResult[value] = append(theSlicesResult[value],key)
	}
	fmt.Printf("%q",theSlicesResult)

}

func threeSum(nums []int) [][]int {
    result := make([][]int,0,10)
	resultMap := make(map[[3]int]int)
    mapNums := make(map[int][]int)
    for i,v := range nums {
        mapNums[v] = append(mapNums[v], i)
    }
	for k,_ := range mapNums{
		for k1,_ := range mapNums {
			if k == k1 {
				continue
			} else {
				newKey := -(k+k1)
				theValue := mapNums[newKey]
				if newKey == k || newKey == k1 {
					if len(theValue) > 1 {
						resultKey := [3]int{k, k1, newKey}
						sort.Ints(resultKey[:])
						resultMap[resultKey] = 0
					} else {
						continue
					}
				} else {
					if _, ok := mapNums[newKey]; ok {
						resultKey := [3]int{k, k1, newKey}
						sort.Ints(resultKey[:])
						resultMap[resultKey] = 0
					}
				}
			}
		}
	}
	for k := range resultMap {
		item := k[:]
		result =append(result, item)
	}
    return result
}

func rotate(matrix [][]int)  {
    mapIndex := make(map[int]int)
    mapValue := make(map[[2]int]int)
    n := len(matrix)
    tempN := n-1
    for i:=0;i<n;i++{
        mapIndex[i] = tempN
        tempN--  
    }
    for key, value := range mapIndex {
        for i:=0; i<n ;i++ {
            mapValue[[2]int{key, i}] = matrix[i][value]
        }
    }
    for i:=0; i< n; i++ {
        for j:=0 ; j<n; j++{
            matrix[i][j] = mapValue[[2]int{i,j}]
        }
    }
}

func groupAnagrams(strs []string) [][]string {
    tempStrs := make([]string,len(strs),len(strs))
    tempStrs = append(tempStrs, strs...)
    arrayIndex := make([][]int, 10, 10)
	result := make([][]string, 10, 10)
    for index,value := range tempStrs {
        tempStrs[index] = SortString(value)
    }
    for index, value := range tempStrs {
        if value == "-" {
            continue
        }
		tempIndex := make([]int, 10, 10)
		tempIndex = append(tempIndex, index)
        for index2, value2 := range tempStrs {
			if value2 == "-" {
				
			}
			if value == value2 {
				tempIndex = append(tempIndex, index2)
				tempStrs[index2] = "-"
			}
            
        }
		tempStrs[index] = "-"
		arrayIndex = append(arrayIndex,tempIndex)
    }
	for i :=0; i< len(arrayIndex);i++ {
		arrayString := make([]string,10 ,10)
		for _, value := range arrayIndex[i] {
			arrayString = append(arrayString, strs[value])
		}
		result = append(result, arrayString)
	}
	return result   
}

func SortString(w string) string {
    s := strings.Split(w, "")
    sort.Strings(s)
    return strings.Join(s, "")
}

func trap(height []int) int {
	max := -9999
	for _,value := range height {
		if max < value {
			max = value
		}
	}
    size := len(height)
	result := 0
	for i := 1; i <= max; i++ {
		tempLayerArr := make([]int,0)
		begin := -1
        zeroAfterBegin := 0
        zeroAfterEnd := 0
		for key,value := range height {
			if value - i >= 0 {
				if begin == -1 {
					begin = key
				}
				tempLayerArr = append(tempLayerArr, i)
			} else {
                if begin != -1 {
                    zeroAfterBegin++
                }
				tempLayerArr = append(tempLayerArr, 0)
			}
		}
        for j:=size-1; j>=0 ; j-- {
			if tempLayerArr[j] != 0 {
				break
			} else {
                zeroAfterEnd++
            }
		}
		result += (zeroAfterBegin - zeroAfterEnd)
	}
	return result
}