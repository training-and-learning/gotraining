package theArray

import (
	"fmt"
)

func ArrayExample(){
	fmt.Println("Array Example")
	scores := [4]int{1,2,3,4}
	var scores2 [10]int
	scores2[0] = 123
	for i :=0; i< len(scores) ; i++{
		fmt.Printf("%v\n", scores[i])
	}
	for index,value := range scores{
		fmt.Printf("Index: %v, Value: %v\n",index,value)
	}
	fmt.Print("------------------")
	for index,value := range scores2{
		fmt.Printf("Index: %v, Value: %v\n",index,value)
	}

}

func ArrayExample2(){
	a := []string{"hello", "world"}
	fmt.Println(a)
	fmt.Printf("%s\n",a)
	fmt.Printf("%q\n",a)

	var a2 [4][3]string
	fmt.Println(len(a2))

	for i := 0 ; i< 4 ; i ++ {
		for j:=0 ; j<3 ; j++ {
			a2[i][j] = fmt.Sprintf("row %d - column %d", i+1, j+1)
		}
	}

	fmt.Println(a2)
	fmt.Printf("%q\n",a2)
}


func ArrayExmaple3(){
	array := [5]int{1, 2, 3, 4, 5}

	mySlice := array[1:3]
	fmt.Println(array)
	fmt.Println(mySlice)

	/**
		Lúc này slice sẽ là 1 con trỏ trỏ tới 1 phần của array
	*/
	mySlice[0] = -89
	fmt.Println(array)
	fmt.Println(mySlice)

	/**
		Sau khi append, cơ chế sẽ là tạo ra 1 mảng mởi với 1 length mới slice sẽ trở tới 
		mảng mới đó cho nên khi thay đổi phần tử của slice sẽ ko thay đổi của array vì 
		slice không còn trỏ tới đó nữa 
	*/

	array2 := []int{7, 8, 9}
	mySlice = append(mySlice, array2...)
	mySlice[0] = -99
	fmt.Println(array)
	fmt.Println(mySlice)
}