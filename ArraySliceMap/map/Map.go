package theMap

import (
	"fmt"
)

func MapExample(){
	fmt.Println("Map Example")
	lookup := make(map[string]int)
	lookup["key1"] = 9001
	power, exists := lookup["key0"]
	fmt.Println(power,exists)

	cities := map[string]int{
		"new_york":8336697,
		"los_angeles":3587799,
		"chicago":2714856,
	}

	for key,value := range cities {
		fmt.Printf("key: %s - value: %d", key,value)
	}

	myMap := make(map[int][]int)
	
	fmt.Println(myMap)
}

func containsNearbyDuplicate(nums []int, k int) bool {
    myMap := make(map[int][2]int{0,0})
    for index, value := range nums {
        if _,ok := myMap[value]; ok {
            if index < myMap[value][1] {
                myMap[value][1] = index
            } 
        } else {
            myMap[value][0] = index
            myMap[value][1] = 0
        }
    }
    for key, value := range myMap {
        if distance := value[1] - value[0]; distance >= 0 && distance <= k {
            return true
        }
    }
    return false
}
