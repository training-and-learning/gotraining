package main

import (
	"fmt"
)

func main() {
	fmt.Println("----------------")
	deferExample()
	fmt.Println("----------------")
	deferExample1()
	fmt.Println("----------------")
	deferExample2()
	fmt.Println("----------------")
	deferAndPanicExample()
	fmt.Println("----------------")
	defer func ()  {
		if err := recover(); err != nil {
			fmt.Println("Handler the error from example: ", err)
		}
	}()
	deferAndPanicExample1()
}



func deferExample(){
	fmt.Println("Line 1")
	fmt.Println("Line 2")
	defer fmt.Println("Line 3")
	fmt.Println("Line 4")
}

func deferExample1() {
	/*
	Stack storage for defer
	*/
	defer fmt.Println("Line 1")
	defer fmt.Println("Line 2")
	defer fmt.Println("Line 3")
	defer fmt.Println("Line 4")
}

func deferExample2(){
	a := 12
	defer fmt.Println("a = ",a)
	a = 24
}

func deferAndPanicExample(){
	/*
	Defer must declare before panic line inorder to recovery
	*/
	defer func ()  {
		if err := recover(); err != nil {
			fmt.Println("Recover from panic error: ", err)
		}
	}()
	panic("Error you created")
}

func deferAndPanicExample1(){
	/*
	Can throw a panic to anorther func to handle it
	*/
	defer func ()  {
		if err := recover(); err != nil {
			fmt.Println("Pass error from example")
			panic(err)
		}
	}()
	panic("Error you created")
}