package contextmodule

import "context"

func TodoContextExample(ctx context.Context, key interface{}) interface{} {
	return ctx.Value(key)
}