package contextmodule

import (
	"context"
	"reflect"
	"testing"
)

var cases = []struct{
	Key interface{}
	Value interface{}
}{
	{Key:"Hello", Value: "World"},
	{Key: 123, Value: "hello"},
	{Key: 123, Value: nil},

}

func TestContextTodoExample(t *testing.T) {
	t.Run("Context with value", func(t *testing.T) {
		for _,testCase := range cases {
			ctx := context.Background()
			ctx = context.WithValue(ctx, testCase.Key, testCase.Value)
			got := TodoContextExample(ctx, testCase.Key)
			if !reflect.DeepEqual(got, testCase.Value){
				t.Errorf("got %q, want %q", got, testCase.Value)
			}
		
		} 
	})
	
}

