package modules

import (
	"fmt"
	"io"
	"math"
	"time"
)

const (
	hourHandLength = 50
	minuteHandLength = 80
	secondHandLength = 90
	clockCenterX = 150
	clockCenterY = 150

	secondsInHalfClock = 30
	secondsInClock     = 2 * secondsInHalfClock
	minutesInHalfClock = 30
	minutesInClock     = 2 * minutesInHalfClock
	hoursInHalfClock   = 6
	hoursInClock       = 2 * hoursInHalfClock
)

const (
	svgStart = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
	<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
	<svg xmlns="http://www.w3.org/2000/svg"
		 width="100%"
		 height="100%"
		 viewBox="0 0 300 300"
		 version="2.0">`
	bezel = `<circle cx="150" cy="150" r="100" style="fill:#fff;stroke:#000;stroke-width:5px;"/>`
	svgEnd = `</svg>`
)

type Point struct {
	X float64
	Y float64
}


func SVGWriter(w io.Writer, tm time.Time) {
	 io.WriteString(w, svgStart)
	 io.WriteString(w, bezel)
	 secondHand(w, tm)
	 minuteHand(w, tm)
	 hourHand(w, tm)
	 io.WriteString(w, svgEnd)
}


func secondHand(w io.Writer, tm time.Time) {
	p := makeHand(secondHandPoint(tm), secondHandLength)
	fmt.Fprintf(w, `<line x1="150" y1="150" x2="%.3f" y2="%.3f" style="fill:none;stroke:#000;stroke-width:3px;"/>`, p.X, p.Y)
}

func minuteHand(w io.Writer, tm time.Time) {
	p := makeHand(minuteHandPoint(tm), minuteHandLength)
	fmt.Fprintf(w, `<line x1="150" y1="150" x2="%.3f" y2="%.3f" style="fill:none;stroke:#f00;stroke-width:3px;"/>`, p.X, p.Y) 
}

func hourHand(w io.Writer, tm time.Time) {
	p := makeHand(hourHandPoint(tm), hourHandLength)
	fmt.Fprintf(w, `<line x1="150" y1="150" x2="%.3f" y2="%.3f" style="fill:none;stroke:#f00;stroke-width:3px;"/>`, p.X, p.Y) 
}

func makeHand(p Point, length float64) Point {
	p = Point{p.X * length, p.Y * length}
	p = Point{p.X, -p.Y}
	p = Point{p.X + clockCenterX, p.Y + clockCenterY}
	return p
}

func secondHandPoint(t time.Time) Point {
	return angleToPoint(secondsInRadians(t))
}

func minuteHandPoint(t time.Time) Point {
	return angleToPoint(minutesInRadians(t))
}

func hourHandPoint(t time.Time) Point {
	return angleToPoint(hoursInRadians(t))
}

func angleToPoint(angle float64) Point {
	x := math.Sin(angle)
	y := math.Cos(angle)
	return Point{x, y}
}

func roughlyEqualFloat64(a,b float64) bool {
	const equalityThreshhold = 1e-7
	return math.Abs(a-b) < equalityThreshhold
}

func roughlyEqualPoint(a,b Point) bool {
	return roughlyEqualFloat64(a.X, b.X) && roughlyEqualFloat64(a.Y, b.Y)
}

func secondsInRadians(tm time.Time) float64 {
	return (math.Pi/ (secondsInHalfClock / (float64(tm.Second()))))
}

func minutesInRadians(t time.Time) float64 {
	return (secondsInRadians(t) / minutesInClock) + (math.Pi/ (minutesInHalfClock / (float64(t.Minute()))))
}

func hoursInRadians(tm time.Time) float64 {
	return (minutesInRadians(tm) / hoursInClock) + (math.Pi / (hoursInHalfClock / float64(tm.Hour() % 12)))
}

func simpleTime(hours, minutes, seconds int) time.Time {
	return time.Date(312, time.October, 28, hours, minutes, seconds, 0, time.UTC)
}

func testName(t time.Time) string {
	return t.Format("15:04:05")
}
