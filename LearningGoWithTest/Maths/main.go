package main

import (
	"fmt"
	"math"
	"mathsmodule/modules"
	"os"
	"time"
)



func main() {
	t := time.Now()
	modules.SVGWriter(os.Stdout, t)
}


func InfinitiZeroExample(){
	// fail to compile
	//fmt.Println(10.0 / 0.0)
	fmt.Println(10.0 / zero())
	zeroNum := 0
	fmt.Println(10.0 / float64(zeroNum))
	fmt.Println(secondsinradian())
}

func zero() float64 {
	return 0.0
}

func secondsinradian() float64 {
	return (math.Pi / (30 / (float64(zero()))))
}

