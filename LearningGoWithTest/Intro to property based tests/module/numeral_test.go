package module

import (
	"fmt"
	"log"
	"testing"
	"testing/quick"
)

var(
	cases = []struct {
		Arabic uint16 
		Roman string
	}{
		{Arabic: 1, Roman: "I"},
		{Arabic: 2, Roman: "II"},
		{Arabic: 3, Roman: "III"},
		{Arabic: 4, Roman: "IV"},
		{Arabic: 5, Roman: "V"},
		{Arabic: 6, Roman: "VI"},
		{Arabic: 7, Roman: "VII"},
		{Arabic: 8, Roman: "VIII"},
		{Arabic: 9, Roman: "IX"},
		{Arabic: 10, Roman: "X"},
		{Arabic: 14, Roman: "XIV"},
		{Arabic: 18, Roman: "XVIII"},
		{Arabic: 20, Roman: "XX"},
		{Arabic: 39, Roman: "XXXIX"},
		{Arabic: 40, Roman: "XL"},
		{Arabic: 47, Roman: "XLVII"},
		{Arabic: 49, Roman: "XLIX"},
		{Arabic: 50, Roman: "L"},
		{Arabic: 100, Roman: "C"},
		{Arabic: 90, Roman: "XC"},
		{Arabic: 400, Roman: "CD"},
		{Arabic: 500, Roman: "D"},
		{Arabic: 900, Roman: "CM"},
		{Arabic: 1000, Roman: "M"},
		{Arabic: 1984, Roman: "MCMLXXXIV"},
		{Arabic: 3999, Roman: "MMMCMXCIX"},
		{Arabic: 2014, Roman: "MMXIV"},
		{Arabic: 1006, Roman: "MVI"},
		{Arabic: 798, Roman: "DCCXCVIII"},
	}
)

func TestRomanNumerals(t *testing.T) {

	// cases := []struct{
	// 	Description string
	// 	Arabic int
	// 	Want string
	// }{
	// 	{"1 get converted to I", 1, "I"},
	// 	{"2 get converted to II", 2, "II"},
	// 	{"3 gets converted to III", 3, "III"},
	// 	{"4 gets converted to IV (can't repeat more than 3 times)", 4, "IV"},
	// 	{"5 gets converted to V", 5, "V"},
	// 	{"9 gets converted to IX", 9, "IX"},
	// 	{"40 gets converted to XL", 40, "XL"},
	// 	{"47 gets converted to XLVII", 47, "XLVII"},
	// 	{"49 gets converted to XLIX", 49, "XLIX"},
	// 	{"50 gets converted to L", 50, "L"},
	// }
	
	// for _, casei := range cases {
	// 	t.Run(casei.Description, func(t *testing.T) {
			
	// 		got := ConvertToRoman(casei.Arabic)

	// 		if got != casei.Want {
	// 			t.Errorf("got %q, want %q", got, casei.Want)
	// 		}
	// 	})
	// }

	

	for _,test := range cases {
		t.Run(fmt.Sprintf("%d gets converted to %q", test.Arabic, test.Roman), func(t *testing.T) {
			got := ConvertToRoman(test.Arabic)
			if got != test.Roman {
				t.Errorf("got %q, want %q", got, test.Roman)
			}
		})
	}
}

func TestConvertingToArabic(t *testing.T) {
	for _,test := range cases[:] {
		t.Run(fmt.Sprintf("%q gets converted to %d", test.Roman, test.Arabic), func(t *testing.T) {
			got := ConvertToArabic(test.Roman)
			if got != test.Arabic {
				t.Errorf("got %d, want %d", got, test.Arabic)
			}

		})
	}
}

func TestPropertiesOfConversion(t *testing.T){
	assertion := func (arabic uint16) bool {
		if arabic > 3999 {
			log.Println(arabic)
			return true
		}
		t.Log("testing", arabic)
		roman := ConvertToRoman(arabic)
		fromRoman := ConvertToArabic(roman)
		return fromRoman == arabic
	}

	if err := quick.Check(assertion, &quick.Config{MaxCount: 1000}); err != nil {
		t.Error("failed checks", err)
	}
}