package mocking

import (
	"bytes"
	"reflect"
	"testing"
	"time"
)

const (
	write = "write"
	sleep = "sleep"
)

type SpySleeper struct {
	Calls int
}

func (spy *SpySleeper) Sleep(){
	spy.Calls++
}



type SpyCountDownOperations struct {
	Calls []string
}

func (s *SpyCountDownOperations) Sleep(){
	s.Calls = append(s.Calls, sleep)
}
/**
implement write io 
type Writer interface {
	Write(p []byte) (n int, err error)
}
*/
func (s *SpyCountDownOperations) Write(p []byte) (n int, err error){
	s.Calls = append(s.Calls, write)
	return
}


type SpyTime struct {
	durationSlept time.Duration
}

func (s *SpyTime) Sleep(duration time.Duration){
	s.durationSlept = duration
}



func TestCountDown(t *testing.T) {
	t.Run("prints 3 to Go !", func(t *testing.T) {
		buffer := &bytes.Buffer{}

		CountDown(buffer, &SpyCountDownOperations{} )

		got:= buffer.String()
		want := `3
2
1
Go !`

		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	})

	t.Run("sleep before every print", func(t *testing.T) {
		spySleepPrinter := &SpyCountDownOperations{}
		CountDown(spySleepPrinter, spySleepPrinter)
		
		want := []string{
			write,
			sleep,
			write,
			sleep,
			write,
			sleep,
			write,
		}

		if !reflect.DeepEqual(want, spySleepPrinter.Calls) {
			t.Errorf("wanted calls %v got %v", want, spySleepPrinter.Calls)
		}
	})
}


func TestConfigurableSleeper(t *testing.T) {
	sleepTime := 5 * time.Second
	spyTime := &SpyTime{}
	sleeper := ConfigurableSleeper{sleepTime, spyTime.Sleep}
	sleeper.Sleep()

	if spyTime.durationSlept != sleepTime {
		t.Errorf("should have slept for %v but slept for %v", sleepTime, spyTime.durationSlept)
	}

}