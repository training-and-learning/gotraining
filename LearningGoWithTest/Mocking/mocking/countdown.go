package mocking

import (
	"fmt"
	"io"
	"time"
)

const (
	finalWord = "Go !"
	countdownStart = 3
)

type ConfigurableSleeper struct {
	duration time.Duration
	sleep func(time.Duration)
}

func (c *ConfigurableSleeper) Sleep(){
	c.sleep(c.duration)
}

type Sleeper interface {
	Sleep()
}

func CountDown(out io.Writer, sleeper Sleeper) {
	/**
	test willfail 
	*/
	// for i:=countdownStart ; i>=1 ; i--{	
	// 	sleeper.Sleep()
	// }
	// for i:=countdownStart ; i>=1 ; i--{	
	// 	fmt.Fprintln(out, i)
	// }

	/**
	test will pass 
	*/
	for i := countdownStart; i >= 1; i-- {
		fmt.Fprintln(out, i)
		sleeper.Sleep()
	}
	fmt.Fprint(out, finalWord)
}