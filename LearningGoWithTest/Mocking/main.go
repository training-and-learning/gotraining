package main

import (
	mock "mocking/mocking"
	"os"
	"time"
)

type DefaultSleepr struct{

}

func (d *DefaultSleepr) Sleep(){
	time.Sleep(time.Second)
}

func main() {
	mock.CountDown(os.Stdout, &DefaultSleepr{})
}