package structmethodinterface

import "testing"

func TestPerimeter(t *testing.T) {
	rectangle := Rectangle{10.0, 10.0}
	got := Perimeter(rectangle)
	want := 40.0

	if got != want {
		t.Errorf("got %.2f want %.2f", got, want)
	}
}

func TestArea(t *testing.T) {

	checkAera := func (t testing.TB, shape Shape,  got, want float64)  {
		t.Helper()
		if got != want {
			t.Errorf("%#v got %g want %g",shape, got, want)
		}
	}

	/*t.Run("rectangles", func(t *testing.T) {
		rectangle := Rectangle{10.0, 10.0}
		checkAera(t, rectangle.Area(), 100.0)
	})

	t.Run("Circles", func(t *testing.T) {
		circle := Circle{10.0}
		checkAera(t, circle.Area(), 314.1592653589793)
	})*/

	// table driven test
	areaTest := []struct{
		name string
		shape Shape
		want float64
	}{
		{name: "Rectangle",shape: Rectangle{12, 6}, want: 72},
		{name: "Circle",shape: Circle{10}, want: 314.1592653589793},
		{name: "Triangle",shape: Triangle{12, 6}, want: 35.0},
	}

	for _, tt := range areaTest {
		t.Run(tt.name, func(t *testing.T) {
			checkAera(t, tt.shape , tt.shape.Area(), tt.want)
		})
	}
}

func BenchmarkPerimeter(b *testing.B) {
	// run go test  -bench=.
	for i := 0; i < b.N; i++ {
		Perimeter(Rectangle{10000.0, 20000.0})
	}
}

func BenchmarkArea(b *testing.B) {
	for i:= 0; i< b.N; i++ {
		Area(Rectangle{10000.0, 20000.0})
	}
}