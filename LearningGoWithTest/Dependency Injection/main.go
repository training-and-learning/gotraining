package main

import (
	"log"
	"net/http"

	di "di/dependency_injection"
)

func MyGreeterHandler(w http.ResponseWriter, r *http.Request){
	di.Greet(w, "word")
}

func main() {
	log.Fatal(http.ListenAndServe(":5001", http.HandlerFunc(MyGreeterHandler)))
}