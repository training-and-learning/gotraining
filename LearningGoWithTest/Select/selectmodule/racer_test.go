package selectmodule

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestRacer(t *testing.T) {
	
	t.Run("return server reponse faster", func(t *testing.T) {
		slowServer := makeDelayedServer(20 * time.Millisecond)
		fastServer := makeDelayedServer(1 * time.Millisecond)
		defer slowServer.Close()
		defer fastServer.Close()

		slowUrl := slowServer.URL
		fastUrl := fastServer.URL
	
		want := fastUrl

		got,err := Racer(slowUrl, fastUrl)

		if err != nil {
			t.Fatalf("did not expect an error but got one %v", err)
		}

		if got != want {
			t.Errorf("got %q, want %q", got, want)
		}
	})

	t.Run("return an error if a server doesn't response within 10s", func(t *testing.T) {
		/**
		this test will end up 2 second because 
		when you export error(1s timeout) to terminal it will run the defer function to close the server
		but right now server is on get request delay 2 second so it will waiting 1s more to done request 
		then it will close the server, 
		you can remove( not recommended) close server to see , the test will done after 1s

		*/
		server := makeDelayedServer(2 * time.Second)
		defer server.Close()

		startTime := time.Now()
		winner, err := ConfigurableRacer(server.URL, server.URL, 1 * time.Second)
		timeCal := time.Since(startTime)
		log.Default().Println(winner, timeCal)

		if err == nil {
			t.Error("expected an error but didn't get one")
		}
	})
}

func makeDelayedServer(delay time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(delay)
		w.WriteHeader(http.StatusOK)
	}))
}