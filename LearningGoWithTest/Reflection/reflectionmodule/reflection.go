package reflectionmodule

import (
	"log"
	"reflect"
)

func Walk(x interface{}, fn func(input string)) {
	val := getValue(x)

	// if val.Kind() == reflect.Slice {
	// 	for i := 0; i < val.Len() ; i++ {
	// 		Walk(val.Index(i).Interface(), fn)
	// 	}
	// 	return
	// }

	// for i := 0; i < val.NumField(); i++ {
	// 	field := val.Field(i)
	// 	log.Default().Printf("field : %v\n", field)
	// 	// if field.Kind() == reflect.String {
	// 	// 	fn(field.String())
	// 	// } 
	// 	// if field.Kind() == reflect.Struct {
	// 	// 	Walk(field.Interface(), fn)
	// 	// }

	// 	switch field.Kind() {
	// 	case reflect.String:
	// 		fn(field.String())
	// 	case reflect.Struct:
	// 		Walk(field.Interface(), fn)
	// 	}
	// }


	// switch val.Kind() {
	// case reflect.Struct:
	// 	for i := 0; i < val.NumField(); i++ {
	// 		Walk(val.Field(i).Interface(), fn)
	// 	}
	// case reflect.Slice:
	// 	for i := 0; i < val.Len(); i++ {
	// 		Walk(val.Index(i).Interface(), fn)
	// 	}
	// case reflect.String:
	// 	fn(val.String())
	// }

	// numberOfValues := 0
	// var getField func(int) reflect.Value

	// switch val.Kind() {
	// case reflect.String:
	// 	fn(val.String())
	// case reflect.Slice, reflect.Array:
	// 	numberOfValues = val.Len()
	// 	getField = val.Index
	// case reflect.Struct:
	// 	numberOfValues = val.NumField()
	// 	getField = val.Field
	// case reflect.Map:
	// 	for _, key := range val.MapKeys() {
	// 		Walk(val.MapIndex(key).Interface(), fn)
	// 	}
	// }
	// for i := 0; i < numberOfValues; i++ {
	// 	Walk(getField(i).Interface(), fn)
	// }

	walkValue := func(value reflect.Value){
		Walk(value.Interface(), fn)
	}

	switch val.Kind() {
	case reflect.String:
		fn(val.String())
	case reflect.Struct:
		for i := 0; i < val.NumField(); i++ {
			walkValue(val.Field(i))
		}
	case reflect.Slice, reflect.Array:
		for i := 0; i < val.Len(); i++ {
			walkValue(val.Index(i))
		}
	case reflect.Map:
		for _, key := range val.MapKeys() {
			walkValue(val.MapIndex(key))
		}
	case reflect.Chan:
		for v, ok:= val.Recv(); ok; v, ok = val.Recv() {
			walkValue(v)
		}
	case reflect.Func:
		funcResults := val.Call(nil)
		for _, res := range funcResults {
			walkValue(res)
		} 
	}
}

func getValue(x interface{}) reflect.Value {
	val := reflect.ValueOf(x)
	log.Default().Printf("val : %v\n", val)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	log.Default().Printf("val : %v\n", val)
	return val
}