package iteration

import "testing"

func TestRepeat(t *testing.T) {
	repeat := Repeat("a")
	want := "aaaaa"

	if repeat != want {
		t.Errorf("expected %s but got %s", want, repeat)
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a")
	}
}