package main

import (
	"context"
	"go-specs-greet/go_specs_greet"
	"go-specs-greet/specifications"
	"log"
	"os/exec"
	"testing"

	"github.com/alecthomas/assert/v2"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

func TestGreeterServer2(t *testing.T) {

	ctx := context.Background()
	cmd := exec.Command("pwd")
	stdout, err := cmd.Output()
	if err != nil {
		log.Fatalln(err)
	}
	path := string(stdout)
	log.Println(path)
	req := testcontainers.ContainerRequest{
		FromDockerfile: testcontainers.FromDockerfile{
			Context:       ".",
			Dockerfile:    "./Dockerfile",
			PrintBuildLog: true,
		},
		ExposedPorts: []string{"8080:8080"},
		WaitingFor:   wait.ForHTTP("/").WithPort("8080"),
	}

	log.Println(req.FromDockerfile.Context)
	log.Println(req.FromDockerfile.Dockerfile)

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started: true,
	})

	assert.NoError(t, err)
	t.Cleanup(func() {
		assert.NoError(t, container.Terminate(ctx))
	})

	driver := go_specs_greet.Driver{BaseURL: "http://localhost:8080"}
	specifications.GreetSpecification(t, driver)
}