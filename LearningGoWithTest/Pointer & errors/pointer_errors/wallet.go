package pointererrors

import (
	"errors"
	"fmt"
)

var ErrInsufficentFunds = errors.New("cannot withdraw, insufficent funds")

type Bitcoin float64

func (b Bitcoin) String() string {
	return fmt.Sprintf("%.2f BTC", b)
}

type Wallet struct {
	balance Bitcoin
}

func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

func (w *Wallet) WithDraw(amount Bitcoin) error{
	if amount > w.balance {
		return ErrInsufficentFunds
	}

	w.balance -= amount
	return nil
}

func (w *Wallet) Balance() Bitcoin {
	return w.balance
}
