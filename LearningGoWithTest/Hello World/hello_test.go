package main

import "testing"

func TestHello(t *testing.T) {
	t.Run("saying hello to people", func(t *testing.T) {
		got := Hello("Chris")
		want := "Hello, Chris"
		assertCorrectMessage(t, got, want)
	})

	t.Run("empty string defaults to 'world'", func(t *testing.T) {
		got := Hello("")
		want := "Hello, world"
		assertCorrectMessage(t, got, want)
	})

	t.Run("empty language", func(t *testing.T) {
		got := Hello("Elodie", "")
		want := "Hello, Elodie"
		assertCorrectMessage(t, got, want)
	})

	t.Run("in Spanish", func(t *testing.T) {
		got := Hello("Elodie", spanish)
		want := "Hola, Elodie"
		assertCorrectMessage(t, got, want)
	})

	t.Run("in French", func(t *testing.T) {
		got := Hello("Franch", french)
		want := "Bonjour, Franch"
		assertCorrectMessage(t, got, want)
	})
}

func assertCorrectMessage(t testing.TB, got,want string){
	t.Helper()
	/**
		Ham này để log line ở function test fail 
		ví dụ có 2 funtion 
		test A
		test B
		2 function này sử dung chung 1 function assertCorrectMessage() ...
		- nếu không có t.Helper khi chạy go test => báo lỗi ở line trong function assertCorretMessage()
		- có t.Helper khi chạy go test => báo lỗi ở test A hoặc test B nếu test A hoặc test B lỗi

	*/
	if got != want {
		t.Errorf("got %q want %q",got,want)
	}
}