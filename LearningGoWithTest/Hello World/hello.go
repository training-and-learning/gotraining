package main

import (
	"fmt"
)

const (
	spanish = "spanish"
	french = "french"
	englishHelloPrefix = "Hello, "
	spanishHelloPrefix = "Hola, "
	frenchHelloPrefix = "Bonjour, "
)


func Hello(name... string) string {
	if name[0] == "" {
		name[0] = "world"
	}
	switch len(name){
	case 1:
		return englishHelloPrefix + name[0]
	case 2:
		return greetingPrefix(name[1]) + name[0]
	}
	return ""
}

func greetingPrefix(language string) (prefix string){
	switch language{
	case french:
		prefix = frenchHelloPrefix
	case spanish:
		prefix = spanishHelloPrefix
	default:
		prefix = englishHelloPrefix
	}
	return
}

func main() {
	fmt.Println(Hello("world"))
}