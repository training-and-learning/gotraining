package arrayandslices

func Sum(numbers []int) int {
	sum := 0
	// for i := 0; i < 5; i++ {
	// 	sum += numbers[i]
	// }
	for _, number := range numbers {
		sum += number
	}
	return sum
}

func SumAll(numbersToSum ...[]int) []int {
	// lengthOfNumbers := len(numbersToSum)
	// sum := make([]int, lengthOfNumbers)
	// for i, numbser := range numbersToSum {
	// 	sum[i] = Sum(numbser)
	// }
	// return sum
	var sum []int
	for _, number := range numbersToSum {
		sum = append(sum, Sum(number))
	}
	return sum
}

func SumAllTails(numbersToSum ...[]int) []int {
	var sum []int
	for _, number := range numbersToSum {
		if len(number) == 0 {
			sum = append(sum, 0)
		} else {
			tail := number[1:]
			sum = append(sum, Sum(tail))
		}
	}
	return sum
}