package concurrency

import (
	"log"
)

type WebsiteChecker func(string) bool
type result struct {
	string
	bool
}

func CheckWebsites(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)
	resultChannel := make(chan result)
	closeChannel := make(chan bool)
	go func() {
		for _, url := range urls {
			resultChannel <- result{ url, wc(url)}
		}
		closeChannel<-true
	}()
	
	Loop: for{
		select{
		case resultCheck := <- resultChannel:
			log.Default().Println(resultCheck)
			results[resultCheck.string] = resultCheck.bool
		case <- closeChannel:
			close(resultChannel)
			break Loop
		}
	}
	return results
}