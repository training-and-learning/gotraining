package main

import (
	"app/src"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"sync"
)

func main() {
	server := &http.Server{
		Addr: ":8080",
	}
	myMux := http.NewServeMux()
	/*myMux.HandleFunc("/home",func(w http.ResponseWriter, r *http.Request) {
		log.Default().Println("Home path")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode("Hello world")
	})*/
	myMux.HandleFunc("/sayhello",func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		filter, present :=  query["name"]
		if !present || len(filter) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("Name is Empty")
			
		} else {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(fmt.Sprintf("Hello %s",filter[0]))
		}
	})
	myMux.HandleFunc("/sayhello2",func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		if name:= query.Get("name"); len(strings.Trim(name," ")) != 0 {
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(fmt.Sprintf("Say hello 2 %v", name))
		} else {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode("Empty request param")
		}
	})
	myMux.HandleFunc("/testpostrequest",func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		defer body.Close()
		data, errData := io.ReadAll(body)
		if errData != nil {
			panic(errData)
		}
		json.NewEncoder(w).Encode(fmt.Sprintf("%v",data))
	})

	myMux.Handle("/custome-handler",new(src.CusHandler))
	myMux.HandleFunc("/home", src.TestMiddleware(func(w http.ResponseWriter, r *http.Request) {
		log.Default().Println("Home path")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode("Hello world")
	}))
	hold := new(sync.WaitGroup)
	hold.Add(1)
	go func ()  {
		server.Handler = myMux
		err := server.ListenAndServe()
		if  err != nil{
			log.Println("Error !!")
			log.Fatal(err.Error())
			hold.Done()
		}	
	}()
	hold.Wait()
}