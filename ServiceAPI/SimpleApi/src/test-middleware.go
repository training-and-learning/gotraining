package src

import (
	"log"
	"net/http"
)

/*

Note Handle và HandleFunc trong package net/http golnag

Handle là 1 interface với phương thức là ServerHTTP
HandleFunc là 1 cách implement interface đó ( xem trong code doc sẽ thấy th này overwrite phương thước ServerHTTP)
Xem file custom-handler.go để biết thêm
*/


func TestMiddleware(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Default().Printf("This is Test Middleware\n")
		h.ServeHTTP(w, r)
	})
}