package src

import (
	"log"
	"net/http"
)

type CusHandler struct{}

func (cusHand CusHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Default().Println("This is custom implement handler")
}