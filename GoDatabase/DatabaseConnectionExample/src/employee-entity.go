package src

import "time"

type Employee struct {
	ID    string `db:"id" json:"id"`
	Name  string `db:"name" json:"name"`
	Birth time.Time `db:"birth" json:"birth"`
	Department string `db:"department" json:"department"`
	Email string `db:"email" json:"email"`
}