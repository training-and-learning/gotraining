package src

import (
	"fmt"
)

type DatabaseHelper struct {
	DatabaseName string
	Username     string
	Password     string
	Addr string
	Port         string
}

func (dbHelper DatabaseHelper) MySQLDatabase() string {
	
	return "mysql"
}

func (dbHelper DatabaseHelper) GetDatasourceUrl() string {
	return fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true", dbHelper.Username, dbHelper.Password, dbHelper.Addr, dbHelper.Port, dbHelper.DatabaseName)
}