package main

import (
	"app/src"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)




func main() {
	envFile, errEnvFile := godotenv.Read(".env")
	if errEnvFile != nil {
		panic(errEnvFile)
	}
	dbHelperObject := &src.DatabaseHelper{
		DatabaseName: envFile["DATABASE_NAME"],
		Username: envFile["USERNAME"],
		Password: envFile["PASSWORD"],
		Addr: "127.0.0.1",
		Port: envFile["DATABASE_PORT_TCP"],
	}

	db, errDb := sql.Open(dbHelperObject.MySQLDatabase(), dbHelperObject.GetDatasourceUrl())

	if errDb != nil {
		panic(errDb)
	}

	defer func ()  {
		log.Default().Println("Close db connection")
		db.Close()
	}()

	
	employeeListResult := make([]src.Employee, 0,10)
	statment,errstm := db.Prepare("SELECT * FROM employee")
	if errstm != nil {
		panic(errstm)
	}
	resultStm, errRs := statment.Query()
	if errRs != nil {
		panic(errRs)
	}
	defer func ()  {
		log.Println("Close result set")
		resultStm.Close()
	}()
	defer func ()  {
		log.Println("Close statement prepare")
		statment.Close()	
	}()
	for resultStm.Next() {
		var employeeItemObj src.Employee
		errScan := resultStm.Scan(
			&employeeItemObj.ID, 
			&employeeItemObj.Name, 
			&employeeItemObj.Department,
			&employeeItemObj.Birth,
			&employeeItemObj.Email,
		)
		if errScan != nil {
			panic(errScan)
		} else {
			employeeListResult = append(employeeListResult, employeeItemObj)
		}
	}
	stringJson,_ := json.Marshal(employeeListResult)
	fmt.Printf("Result query: %v\n",string(stringJson))

}