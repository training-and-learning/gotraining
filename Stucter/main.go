package main

import (
	"fmt"
	"stucter/code"
)

func main(){
	fmt.Println("Hello world")

	var intPointer *int = new(int)
	fmt.Printf("%v", *intPointer)
	person1:=code.NewPerson("asd","asd","023512",12)
	person1.IncreaseAge()
	person1.PrintInfo()
	person2 := code.Person{}
	person2.Name = "asdqwe"
	person2.Address = "65q/weq"
	person2.Age = 123
	person2.Phone = "123213"
	person2.IncreaseAge()
	person2.PrintInfo()


	student1 := code.Student{
		Person: &code.Person{
			Name: "123213",
			Address: "12s/123",
			Phone: "02435",
			Age: 1232,
		},
		ClassId: "NP01",
	}
	student1.PrintInfo()
	fmt.Printf("%v",student1.ClassId)

	student2 := new(code.Student)
	student2.Person = new(code.Person)
	student2.Name = "tytry213"
	student2.Address = "qwe32"
	student2.Phone = "0745132"
	student2.Age = 23
	student2.ClassId = "123s"
	student2.IncreaseAge()
	student2.PrintInfo()
	fmt.Printf("%v",student2.GetClassID())
}