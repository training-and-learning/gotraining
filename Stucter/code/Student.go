package code

type Student struct {
	*Person
	ClassId string
}

func (stu *Student) GetClassID() string {
	return stu.ClassId
}