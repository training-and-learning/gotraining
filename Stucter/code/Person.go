package code

import (
	"fmt"
)

type Person struct {
	Name    string
	Address string
	Phone   string
	Age     uint16
}

func NewPerson(name, address, phone string, age uint16) *Person {
	return &Person{
		Name:    name,
		Address: address,
		Phone:   phone,
		Age:     age,
	}
}

func (p *Person) IncreaseAge() {
	p.Age++
}

func (p *Person) PrintInfo(){
	fmt.Printf("Name: %v\nAddress:%v\nPhone: %v\nAge: %v\n",p.Name,p.Address,p.Phone,p.Age)
}
